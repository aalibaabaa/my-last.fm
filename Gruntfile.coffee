module.exports = ( grunt ) ->

    # Project configuration
    grunt.initConfig(
        pkg: grunt.file.readJSON( "package.json" )

        sourcePath: "src/"

        publicPath: "public/"

        # compile coffee
        coffee:
            compile:
                files: [
                    "index.js": "<%=sourcePath%>index.coffee"
                    "<%=publicPath%>apps/userEventsApp/index.js": "<%=sourcePath%>apps/userEventsApp/index.coffee"
                ]

        # watch file
        watch:
            files: [
                "<%=sourcePath%>**/*.coffee"
                "<%=sourcePath%>**/*.js"
                "<%=sourcePath%>**/*.scss"
                "<%=sourcePath%>**/*.css"
                "views/**/*.html"
            ]
            tasks: [ "build" ]

        # copy files
        copy:
            js:
                expand: true
                cwd: "<%=sourcePath%>lib/"
                src: "*.js"
                dest: "<%=publicPath%>lib/"
            css:
                expand: true
                cwd: "<%=sourcePath%>styles/"
                src: "*.css"
                dest: "<%=publicPath%>styles/"

        # clean public folder
        clean:
            folder: "<%=publicPath%>"

        # compile scss into css
        sass:
            dist:
                files:
                    "<%=publicPath%>styles/custom.css":"<%=sourcePath%>styles/custom.scss"

    )

    # The watcher
    grunt.loadNpmTasks( "grunt-contrib-coffee" )
    # The watcher
    grunt.loadNpmTasks( "grunt-contrib-watch" )
    # The copier
    grunt.loadNpmTasks( "grunt-contrib-copy" )
    # The cleaner
    grunt.loadNpmTasks( "grunt-contrib-clean" )
    # Sass compiler
    grunt.loadNpmTasks( "grunt-contrib-sass" )


    # Grunt tasks
    grunt.registerTask( "build", [ "clean", "copy",  "coffee", "sass" ] )
    grunt.registerTask( "default", [ "clean", "copy", "coffee", "sass", "watch" ] )