My-last.fm
==============
# General
## Project description
This small project fetches data from the [last.fm API](http://www.lastfm.de/api) and shows visited events in a leaflet map.
See live-example on [nodedienst.de:3001](http://nodedienst.de:3001)

# Technical description
## Scripts & libraries
- [node.js](http://nodejs.org/)
- [npm](https://npmjs.org)
- [CoffeeScript](http://coffeescript.org/)
- [grunt.js](http://gruntjs.com/)
- [Bootstrap](http://twitter.github.io/bootstrap/)
- [ExpressJS](http://expressjs.com/)
- [lastfm (npm package)](https://npmjs.org/package/lastfm)
- [Leaflet](http://leafletjs.com/)
- [Handlebars](http://handlebarsjs.com/)
- [jQuery](http://jquery.com/)
- [Sass](http://sass-lang.com/)

## How to use
#### Needed instalations
- Node.js
- npm
- ruby-sass
#### Shell
```shell
$ git clone git@bitbucket.org:aalibaabaa/my-last.fm.git lastfm
$ cd lastfm
$ npm install -d
$ cp config.template.json config.json #add your own credentials
$ grunt build
$ node index
```
### Browser
http://localhost:3001

## Change log
### 0.3.0
- Added homepage & artist search
- Added responsive design

### 0.2.0
- Added first draft of artistSearch
- Added Sass
- Removed userInfoApp
- Added handlebars partials

### 0.1.0
- first draft