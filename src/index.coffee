express     = require( "express" )
app         = express()
LastFmNode  = require( "lastfm" ).LastFmNode
config      = require( "./config.json" )
hbs         = require( "hbs" )
_           = require( "underscore" )

# initialze lastfm
lastfm = new LastFmNode(
    api_key: config.lastfm.api_key
    secret: config.lastfm.secret
)

app.configure( () ->
    app.set( "views", __dirname + "/views" )
    hbs.registerPartials( __dirname + "/views/partials" )
    app.set( "view engine", "html" )
    app.engine( "html", hbs.__express)
    app.use( express.static( __dirname + "/public" ) )
)

app.get( "/", ( req, res ) ->
    res.render( "index", {isHomepage: true} )
)

app.get( "/userEvents", ( req, res ) ->
    res.render( "userEventsApp", {isUserEvents:true} )
)

app.get( "/artistSearch", ( req, res )->
    res.render( "artistSearch", {isArtistSearch:true} )
)


app.get( "/api/user/:userName/:apiMethod" , ( req, res ) ->
    userName = req.params.userName
    apiMethod = req.params.apiMethod
    lastfm.request( apiMethod, {
        user: userName
        handlers:
            success: ( data ) ->
                res.send( data )
                console.log( "Success: #{apiMethod}" )
            error: ( error ) ->
                console.log( "Error: #{userName} #{apiMethod} - #{error.message}" )
    })
)
app.get( "/api/artist/search/:q", ( req, res ) ->
    searchQuery = req.params.q
    lastfm.request( "artist.search", {
        artist: searchQuery
        handlers:
            success: ( data ) ->
                results = []
                mbidList = []
                i = 0
                for artist in data.results.artistmatches.artist
                    if artist.mbid
                        results.push( artist.name )
                        mbidList.push( artist.mbid )

                list = _.object( results, mbidList )
                res.send( { options: results, list: list } )
            error: ( error ) ->
                console.log( "Error: #{searchQuery} - #{error.message}" )
    })
)

app.listen( config.server.port )
console.log( "Server is running on port #{config.server.port}" )