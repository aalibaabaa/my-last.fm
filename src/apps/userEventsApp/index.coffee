#######################################################################################################################
# Collection & Model                                                                                                  #
#                                                                                                                     #
#######################################################################################################################

UserEventModel = Backbone.Model.extend()

UserEventsCollection = Backbone.Collection.extend(
    model: UserEventModel
    url: ()->
        "/api/user/#{@user}/#{@method}"
    user: "zipflklatscher" #karawitz
    method: "user.getPastEvents"

    parse: ( response )->
        return response.events.event
)

#######################################################################################################################
# VIEWS                                                                                                               #
#                                                                                                                     #
######################################################################################################################
EventView = Backbone.View.extend(
    initialize: ()->
        @markerSet = false

    tagName: "tr"

    events: {
        "click .btn": "_setMapPin"
    }

    render: ()->
        model = @model.toJSON()
        eventDate = @_getDateObject( model.startDate ).toLocaleString()
        @$el.html( "<td><strong>#{model.artists.headliner}</strong></td>
                    <td>#{model.venue.name} / #{model.venue.location.city}</td>
                    <td><span class=\"user-events-app-event-date\">#{eventDate}</span>
                    <button class=\"btn btn-small btn-primary pull-right\" type=\"button\">show <span class=\"user-events-app-button\">on map</span> </button></td>")


    _setMapPin: ( e )->
        model = @model.toJSON()
        unless @markerSet
            marker = L.marker( [model.venue.location["geo:point"]["geo:lat"], model.venue.location["geo:point"]["geo:long"]] ).addTo( map )
            marker.bindPopup( @_setPopup( model ) )
            @markerSet = true
        map.setView([model.venue.location["geo:point"]["geo:lat"], model.venue.location["geo:point"]["geo:long"]], 13)
        $element = @$el.find( e.target )
        jQuery( "html, body" ).animate( { scrollTop: ( $element.position().top) * -1 + "px" }, "slow" )
        #console.log( $element.position().top )


    _setPopup: ( model )->
        eventDate = @_getDateObject( model.startDate ).toLocaleString()
        popup = "<div>
                    <strong>#{model.artists.headliner}</strong><br />
                    #{eventDate}<br />
                    \"#{model.title}\"
                </div>"
        return popup

    ##
    # Sat, 01 Jun 2013 17:57:01
    ##
    _getDateObject: ( date )->
        return new Date( date )

)

UserEventsView = Backbone.View.extend(
    initialize: ()->

    render: ()->
        events = @_prepareData( @collection )
        @$el.append( "<table>" )
        _.each( events, ( event )=>
            @$el.find( "table" ).append( @_addEvent( event ) )
        )
        @$el.find( "table" ).addClass( "table table-hover" )


    _prepareData: ( collection )->
        return collection.models


    _addEvent: ( event )->
        evt = new EventView( {
            model: event
        } )
        return evt.render()
)




#######################################################################################################################
# Initialize and start App                                                                                            #
#                                                                                                                     #
#######################################################################################################################
UserEventsCollection = new UserEventsCollection()
EventsView = new UserEventsView({
    el: "#appContainer"
    collection: UserEventsCollection

})

UserEventsCollection.fetch( {
    success: ( collection ) ->
        EventsView.render() # Startpoint of the app
    error: ( error ) ->
        console.log( "Error: #{error.message}" )
} )
